﻿using System;
using System.Text;
using Xunit;

namespace Salt.Interop.Tests
{
	public class InteropTests
	{
		[Fact]
		public void GenericHashTests()
		{
			byte[] key = new byte[GenericHash.Keybytes.ToUInt64()];
			RandomBytes.Buffer(key, GenericHash.Keybytes);

			GenericHashState state = new GenericHashState();
			GenericHash.Init(ref state, key, GenericHash.Keybytes, GenericHash.Bytes);

			byte[] vector = Encoding.ASCII.GetBytes("FooBarBaz");
			GenericHash.Update(ref state, vector, (ulong)vector.Length);

			byte[] outputMultiPart = new byte[GenericHash.Bytes.ToUInt64()];
			GenericHash.Final(ref state, outputMultiPart, (UIntPtr)outputMultiPart.LongLength);

			byte[] outputSinglePart = new byte[GenericHash.Bytes.ToUInt64()];
			GenericHash.Hash(
				outputSinglePart, (UIntPtr)outputSinglePart.LongLength,
				vector, (ulong)vector.Length,
				key, GenericHash.Keybytes
			);

			Assert.Equal(outputSinglePart, outputMultiPart);
		}
	}
}
