﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

[assembly:InternalsVisibleTo("Salt.NET.Tests")]

namespace Salt.Interop
{
	internal static class RandomBytes
	{
		static RandomBytes()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
		}

		[DllImport("libsodium", EntryPoint = "randombytes_random")]
		internal static extern uint Random();

		[DllImport("libsodium", EntryPoint = "randombytes_uniform")]
		internal static extern uint Random(uint upperBound);

		[DllImport("libsodium", EntryPoint = "randombytes_buf")]
		internal static extern void Buffer(byte[] buffer, UIntPtr bufferLength);

		[DllImport("libsodium", EntryPoint = "randombytes_close")]
		internal static extern int Close();

		[DllImport("libsodium", EntryPoint = "randombytes_stir")]
		internal static extern void Stir();

	}

	internal static class SecretBox
	{
		static unsafe SecretBox()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			Keybytes = GetKeybytes();
			Noncebytes = GetNoncebytes();
			Zerobytes = GetZerobytes();
			Macbytes = GetMacbytes();
			Primitive = new string(GetPrimitive());
		}

		internal static UIntPtr Keybytes { get; private set; }

		internal static UIntPtr Noncebytes { get; private set; }

		internal static UIntPtr Zerobytes { get; private set; }

		internal static UIntPtr Macbytes { get; private set; }

		internal static string Primitive { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_secretbox_easy")]
		internal static extern int Easy(byte[] cipherText, byte[] message, UIntPtr messageLength, byte[] nonce, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_secretbox_open_easy")]
		internal static extern int OpenEasy(byte[] message, byte[] cipherText, UIntPtr cipherTextLength, byte[] nonce, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_secretbox_keybytes")]
		private static extern UIntPtr GetKeybytes();

		[DllImport("libsodium", EntryPoint = "crypto_secretbox_noncebytes")]
		private static extern UIntPtr GetNoncebytes();

		[DllImport("libsodium", EntryPoint = "crypto_secretbox_zerobytes")]
		private static extern UIntPtr GetZerobytes();

		[DllImport("libsodium", EntryPoint = "crypto_secretbox_macbytes")]
		private static extern UIntPtr GetMacbytes();

		[DllImport("libsodium", EntryPoint = "crypto_secretbox_primitive")]
		private static extern unsafe sbyte* GetPrimitive();

	}

	internal static class GenericHash
	{
		static unsafe GenericHash()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			BytesMin = GetBytesMin();
			BytesMax = GetBytesMax();
			Bytes = GetBytes();
			KeybytesMin = GetKeybytesMin();
			KeybytesMax = GetKeybytesMax();
			Keybytes = GetKeybytes();
			Statebytes = GetStatebytes();
			Primitive = new string(GetPrimitive());
		}

		internal static UIntPtr BytesMin { get; private set; }

		internal static UIntPtr BytesMax { get; private set; }

		internal static UIntPtr Bytes { get; private set; }

		internal static UIntPtr KeybytesMin { get; private set; }

		internal static UIntPtr KeybytesMax { get; private set; }

		internal static UIntPtr Keybytes { get; private set; }

		internal static UIntPtr Statebytes { get; private set; }

		internal static string Primitive { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_generichash")]
		internal static extern int Hash(byte[] output, UIntPtr outputLength, byte[] input, ulong inputLength, byte[] key, UIntPtr keyLength);

		[DllImport("libsodium", EntryPoint = "crypto_generichash_init")]
		internal static extern int Init(ref GenericHashState state, byte[] key, UIntPtr keyLength, UIntPtr outputLength);

		[DllImport("libsodium", EntryPoint = "crypto_generichash_update")]
		internal static extern int Update(ref GenericHashState state, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_generichash_final")]
		internal static extern int Final(ref GenericHashState state, byte[] output, UIntPtr outputLength);

		[DllImport("libsodium", EntryPoint = "crypto_generichash_bytes_min")]
		private static extern UIntPtr GetBytesMin();

		[DllImport("libsodium", EntryPoint = "crypto_generichash_bytes_max")]
		private static extern UIntPtr GetBytesMax();

		[DllImport("libsodium", EntryPoint = "crypto_generichash_bytes")]
		private static extern UIntPtr GetBytes();

		[DllImport("libsodium", EntryPoint = "crypto_generichash_keybytes_min")]
		private static extern UIntPtr GetKeybytesMin();

		[DllImport("libsodium", EntryPoint = "crypto_generichash_keybytes_max")]
		private static extern UIntPtr GetKeybytesMax();

		[DllImport("libsodium", EntryPoint = "crypto_generichash_keybytes")]
		private static extern UIntPtr GetKeybytes();

		[DllImport("libsodium", EntryPoint = "crypto_generichash_statebytes")]
		private static extern UIntPtr GetStatebytes();

		[DllImport("libsodium", EntryPoint = "crypto_generichash_primitive")]
		private static extern unsafe sbyte* GetPrimitive();

	}

	internal static class Sha256
	{
		static Sha256()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			Bytes = GetBytes();
		}

		internal static UIntPtr Bytes { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha256")]
		internal static extern int Hash(byte[] output, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha256_init")]
		internal static extern int Init(ref Sha256State state);

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha256_update")]
		internal static extern int Update(ref Sha256State state, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha256_final")]
		internal static extern int Final(ref Sha256State state, byte[] output);

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha256_bytes")]
		private static extern UIntPtr GetBytes();

	}

	internal static class Sha512
	{
		static Sha512()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			Bytes = GetBytes();
		}

		internal static UIntPtr Bytes { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha512")]
		internal static extern int Hash(byte[] output, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha512_init")]
		internal static extern int Init(ref Sha512State state);

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha512_update")]
		internal static extern int Update(ref Sha512State state, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha512_final")]
		internal static extern int Final(ref Sha512State state, byte[] output);

		[DllImport("libsodium", EntryPoint = "crypto_hash_sha512_bytes")]
		private static extern UIntPtr GetBytes();

	}

	internal static class HmacSha256
	{
		static HmacSha256()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			Bytes = GetBytes();
		}

		internal static UIntPtr Bytes { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha256")]
		internal static extern int Hash(byte[] output, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha256_init")]
		internal static extern int Init(ref HmacSha256State state);

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha256_update")]
		internal static extern int Update(ref HmacSha256State state, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha256_final")]
		internal static extern int Final(ref HmacSha256State state, byte[] output);

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha256_bytes")]
		private static extern UIntPtr GetBytes();

	}

	internal static class HmacSha512
	{
		static HmacSha512()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			Bytes = GetBytes();
		}

		internal static UIntPtr Bytes { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha512")]
		internal static extern int Hash(byte[] output, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha512_init")]
		internal static extern int Init(ref HmacSha512State state);

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha512_update")]
		internal static extern int Update(ref HmacSha512State state, byte[] input, ulong inputLength);

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha512_final")]
		internal static extern int Final(ref HmacSha512State state, byte[] output);

		[DllImport("libsodium", EntryPoint = "crypto_auth_hmacsha512_bytes")]
		private static extern UIntPtr GetBytes();

	}

	internal static class Salsa20
	{
		static Salsa20()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			Keybytes = GetKeybytes();
			Noncebytes = GetNoncebytes();
		}

		internal static UIntPtr Keybytes { get; private set; }

		internal static UIntPtr Noncebytes { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_stream_salsa20")]
		internal static extern int Random(byte[] output, ulong outputLength, byte[] nonce, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_salsa20_xor")]
		internal static extern int Xor(byte[] cipherText, byte[] message, ulong messageLength, byte[] nonce, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_salsa20_xor_ic")]
		internal static extern int Xor(byte[] cipherText, byte[] message, ulong messageLength, byte[] nonce, ulong counter, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_salsa20_keybytes")]
		private static extern UIntPtr GetKeybytes();

		[DllImport("libsodium", EntryPoint = "crypto_stream_salsa20_noncebytes")]
		private static extern UIntPtr GetNoncebytes();

	}

	internal static class XSalsa20
	{
		static XSalsa20()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			Keybytes = GetKeybytes();
			Noncebytes = GetNoncebytes();
		}

		internal static UIntPtr Keybytes { get; private set; }

		internal static UIntPtr Noncebytes { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_stream_xsalsa20")]
		internal static extern int Random(byte[] output, ulong outputLength, byte[] nonce, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_xsalsa20_xor")]
		internal static extern int Xor(byte[] cipherText, byte[] message, ulong messageLength, byte[] nonce, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_xsalsa20_xor_ic")]
		internal static extern int Xor(byte[] cipherText, byte[] message, ulong messageLength, byte[] nonce, ulong counter, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_xsalsa20_keybytes")]
		private static extern UIntPtr GetKeybytes();

		[DllImport("libsodium", EntryPoint = "crypto_stream_xsalsa20_noncebytes")]
		private static extern UIntPtr GetNoncebytes();

	}

	internal static class ChaCha20
	{
		static ChaCha20()
		{
			UnmanagedLibrary.AcquireHandle("libsodium");
			Keybytes = GetKeybytes();
			Noncebytes = GetNoncebytes();
		}

		internal static UIntPtr Keybytes { get; private set; }

		internal static UIntPtr Noncebytes { get; private set; }

		[DllImport("libsodium", EntryPoint = "crypto_stream_chacha20")]
		internal static extern int Random(byte[] output, ulong outputLength, byte[] nonce, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_chacha20_xor")]
		internal static extern int Xor(byte[] cipherText, byte[] message, ulong messageLength, byte[] nonce, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_chacha20_xor_ic")]
		internal static extern int Xor(byte[] cipherText, byte[] message, ulong messageLength, byte[] nonce, ulong counter, byte[] key);

		[DllImport("libsodium", EntryPoint = "crypto_stream_chacha20_keybytes")]
		private static extern UIntPtr GetKeybytes();

		[DllImport("libsodium", EntryPoint = "crypto_stream_chacha20_noncebytes")]
		private static extern UIntPtr GetNoncebytes();

	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 64)]
	internal struct GenericHashState
	{
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		internal ulong[] H;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
		internal ulong[] T;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
		internal ulong[] F;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
		internal byte[] Buffer;

		internal UIntPtr BufferLength;

		internal byte LastNode;

	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
	internal struct Sha256State
	{
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		internal uint[] State;

		internal ulong Count;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		internal byte[] Buffer;

	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
	internal struct Sha512State
	{
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		internal ulong[] State;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
		internal ulong[] Count;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
		internal byte[] Buffer;

	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
	internal struct HmacSha256State
	{
		internal Sha256State InnerContext;

		internal Sha256State OuterContext;

	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
	internal struct HmacSha512State
	{
		internal Sha512State InnerContext;

		internal Sha512State OuterContext;

	}

}
